/*program exercicio01; {simples}
uses crt;
var raio, area : real;
begin
clrscr;
raio := 0; {inicializar as variaveis}
area := 0;
write ('Informe o raio do circulo: ');
readln (raio);
area := 3.14 * (Sqr (raio)); //Pi = 3.14
writeln ('A area do circulo eh: ', area:6:2); {Formatar a saida, sendo q 6 eh
o num total de caracteres e 2, a quantidade de caracteres depois do ponto}
end.*/


import java.util.Scanner;

public class Exercicio1 {
	//Declaracao de variaveis globais (static)
			static Scanner tecla = new Scanner(System.in);
			
public void main(String[] args) {
			
			//Declaracao de variaveis locais e constantes
			double raio, area;
			final double PI = 3.14;
			
			//Entrada de dados
			System.out.println("Digite o valor do raio: ");
			raio = tecla.nextDouble();
			
			//Processamento de dados
			area = PI * Math.pow(raio,2);
			
			//area = Math.PI * Math.pow(raio,2);
			
			//Sa�da da informa��o
			System.out.println("�rea1 do c�rculo: " + area);
		}
	}

