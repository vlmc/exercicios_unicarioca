import java.util.Scanner;

	public class Registros_1 {
		static Scanner tecla = new Scanner(System.in);
		
		public static String nome;
		public static String endereco;
		public static String telefone;
		
		public static void main(String[] args) {
			
			Registros_1_Dados cliente1;
		    cliente1 = new Registros_1_Dados();
			//Type
			System.out.println("Cadastrar dois clientes: ");
			
			System.out.println("Cliente 1 Nome: ");
				nome = tecla.nextLine();
			System.out.println("Cliente 1 Endereco: ");
				endereco = tecla.nextLine();
			System.out.println("Cliente 1 Telefone: ");
				telefone = tecla.nextLine();
		
		    cliente1.setNome(nome);
		    cliente1.setEndereco(endereco);
		    cliente1.setTelefone(telefone);
			
		    
		    //cliente 2
			Registros_1_Dados cliente2;
		    cliente2 = new Registros_1_Dados();
			
			System.out.println("Cliente 2 Nome: ");
				nome = tecla.nextLine();
			System.out.println("Cliente 2 Endereco: ");
				endereco = tecla.nextLine();
			System.out.println("Cliente 2 Telefone: ");
				telefone = tecla.nextLine();
		
			cliente2.setNome(nome);
			cliente2.setEndereco(endereco);
			cliente2.setTelefone(telefone);
			
			System.out.println("======== Cliente 1 ========");
			System.out.println(cliente1.getNome());
		    System.out.println(cliente1.getEndereco());
		    System.out.println(cliente1.getTelefone());
		    System.out.println("======== Cliente 2 ========");
		    System.out.println(cliente2.getNome());
		    System.out.println(cliente2.getEndereco());
		    System.out.println(cliente2.getTelefone());
		    
		}
		
		
}
